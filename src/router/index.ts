import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import UsersView from "../views/UsersView.vue";
import PosView from "../views/pos/PosView.vue";
import LoginView from "../views/LoginView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: HomeView,
      meta: {
        layout: "MainLayout",
        requireAuth: true,
      },
    },
    {
      path: "/users",
      name: "users",
      component: UsersView,
      meta: {
        layout: "MainLayout",
        requireAuth: true,
      },
    },
    {
      path: "/pos",
      name: "pos",
      component: PosView,
      meta: {
        layout: "MainLayout",
        requireAuth: true,
      },
    },
    {
      path: "/login",
      name: "login",
      component: LoginView,
      meta: {
        layout: "FullLayout",
      },
    },
  ],
});

function isLogin() {
  const user = localStorage.getItem("user");
  return !!user;
}

router.beforeEach((to, from, next) => {
  if (to.meta.requireAuth && !isLogin()) {
    // Redirect to login page if authentication is required and user is not logged in
    next("/login");
  } else {
    // Continue with the navigation
    next();
  }
});

export default router;
